// #ifdef VUE3
import { createSSRApp } from 'vue';
import App from './App';

// 全局注册 uni-icons
import UniIcons from '@/components/uni-icons/uni-icons.vue';

export function createApp() {
  const app = createSSRApp(App);

  // 全局注册组件
  app.component('uni-icons', UniIcons);

  return {
    app,
  };
}
// #endif
